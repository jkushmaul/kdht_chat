Use chance to determine if should forward - makes it non deterministic, even allows for timeouts.
(int)(Rand() * 9) == 4 is a 50% chance to forward

https://docs.google.com/document/d/1WOs9JEHBmyfpP2Txtl1u_GqN4UFQiRQuxv4I4mdK1ys/edit?usp=sharing

DHT messenger
No encryption to start
User has an ID for every friend
To become friends, uses generate an ID, and exchange it once, marking that pair of IDs with some label.
IDs are random uuid
A message is { msgid, timestampms64, type, text }
Msgid is uuid, Type can be text or ack
To send a message:
Append this message to tail of remoteFriendsMessages local value
Send this local value to key=remoteFriendID
To check for incoming messages
For each friend,
Check myFriendID for value
Import new messages since last seen.
For all type==ack messages
Mark internal storage that message was read
For all type==text messages
Send ack message to remoteFriendID as type=ack
What are things I want to get status updates on, progress bar like stuff?
Bootstrapping - I want to be able to get an idea that things are happening
Metrics
Ability to get the metrics on a find_node call, and store_value call.
If refreshing, garbage collecting, republishing, refreshing
Status on the total progress in the loop
What’s next?
Encryption of course - gpg would be easiest for me just from familiarity but could potentially start with something simpler like symmetric encryption - key associated to friendId
I almost want to just use this to improve dht then throw this away and start over with more thought right from the beginning.


This main will run an interactive command line interface.
The commands are 

messages check
messages send
messages republish
messages show
messages show <friend>
messages read <id>

I want this to all happen over tor l
