#ifndef CONTACT_H
#define CONTACT_H
#include <stdint.h>

#include <khashmap.h>
#include <kdht_node.h>


typedef  struct  contact_t {
	kdht_node_t local;
	kdht_node_t remote;
	char label[255];
	uint64_t last_active_tsms;
} contact_t;



void contact_key(khashmap_key_t *key, char *label);
void contact_fill_remote_node(contact_t *contact, kdht_node_t *node);
void contact_fill_local_node(contact_t *contact, kdht_node_t *node);

#endif
