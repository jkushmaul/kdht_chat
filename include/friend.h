#ifndef FRIEND_H
#define FRIEND_H

#include <klist.h>

typedef  struct {
	char id[32];
} chat_id_t;

typedef struct {
	long timestamp_ms;
	chat_id_t friend_id;
	char message[1024];
	int acked;
} message_t;

typedef struct {
	char name[256];
	char notes[512];
	//message_t
	klist_t sent;
	klist_t inbox;

} friend_t;

#endif
