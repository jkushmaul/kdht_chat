#ifndef CHAT_H
#define CHAT_H

#include <kindexedmap.h>

#include <kdht_service.h>

#include <contact.h>

typedef struct {
	uint64_t id;
	uint64_t timestamp_ms;

	char *message;
} message_t;
typedef struct {
	kdht_node_t contact_id;
	kindexedmap_t messages;
} contact_message_container_t;

typedef struct {
	kdht_service_t dht;
	kindexedmap_t contacts;
	khashmap_t *contact_messages;
} chat_t;

int chat_init(chat_t *chat, knet2_ip_handler_t *net);
void chat_release_frees(chat_t  *chat);
contact_t * chat_contact_add(chat_t *chat, char *name, kdht_node_t *local_id, kdht_node_t *remote_id);
contact_t *chat_contact_rm(chat_t *chat, char * name);
int chat_contact_ls(chat_t *chat, contact_t *contact);


int chat_contact_check_messages(chat_t *chat, contact_t *contact);
int chat_contact_send_message(chat_t *chat, contact_t *contact, char *message);
contact_message_container_t *chat_contact_get_messages(chat_t *chat, contact_t *contact);
void chat_collect_found_answers(kservice_t *chat, void *context);
#endif
