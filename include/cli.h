#ifndef CLI_H
#define CLI_H

#include <pthread.h>

#include <knet2_epoll.h>
#include <konfig2.h>
#include <kli.h>
#include <kdht_service.h>

#include "chat.h"

typedef struct {
	knet2_epoll_handler_t net;
	konfig2_t konfig;
	kli_t cli;
	pthread_t cli_thread;
	chat_t chat;
}context_t;


void *cli_thread(void * args);
void cli_process_thread_input(kservice_t *service, void *vcontext);

void cli_init(kli_t *cli, context_t *context);
#endif
