#include <string.h>

#include <kencode.h>

#include "contact.h"

void contact_key(khashmap_key_t *key, char *label)
{
	memset(key, 0, sizeof(khashmap_key_t));
	key->data = (uint8_t*)label;
	key->len = strlen(label);
}

void contact_fill_remote_node(contact_t *contact, kdht_node_t *node)
{
	kdht_node_copy(&contact->remote, node);
}

void contact_fill_local_node(contact_t *contact, kdht_node_t *node)
{
	kdht_node_copy(&contact->local, node);
}
