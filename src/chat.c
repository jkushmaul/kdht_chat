#include <string.h>
#include <stdio.h>

#include <kstring.h>

#include "chat.h"

int chat_contact_compare(void *left, void *right)
{
	contact_t *cleft = (contact_t*)left;
	contact_t *cright = (contact_t*)right;

	return strcmp(cleft->label, cright->label);
}

int chat_message_compare(void *left, void *right)
{
	message_t *cleft = (message_t*)left;
	message_t *cright = (message_t*)right;

	return cleft->timestamp_ms - cright->timestamp_ms;
}

int chat_init(chat_t *chat, knet2_ip_handler_t *net)
{
	memset(chat, 0, sizeof(chat_t));
	kindexedmap_init_allocs(&chat->contacts, chat_contact_compare, 1000);
	chat->contact_messages = khashmap_new_allocs(1000);
	kdht_service_init(&chat->dht, net);
	return 0;
}

void chat_release_frees(chat_t  *chat)
{
	KLIST_FOR_EACH_NODE(node, &chat->contacts.index.list) {
		contact_t *contact = (contact_t*)node->value;

		free(contact);
	}
	khashmap_entry_t *entry = NULL;
	KHASHMAP_FOR_EACH_VALUE(chat->contact_messages, entry) {
		contact_message_container_t *cm = entry->value;

		KLIST_FOR_EACH_NODE(node, &cm->messages.index.list) {
			message_t *message = node->value;

			free(message->message);
			free(message);
		}
		kindexedmap_release_frees(&cm->messages);
		free(cm);
	}
	khashmap_release_frees(chat->contact_messages);
	kindexedmap_release_frees(&chat->contacts);
	kdht_service_release_frees(&chat->dht);
}

contact_t * chat_contact_add(chat_t *chat, char *name, kdht_node_t *local_id, kdht_node_t *remote_id)
{
	contact_t *contact = (contact_t*)calloc(1, sizeof(contact_t));

	sprintf(contact->label, "%s", name);
	kdht_node_copy(local_id, &contact->local);
	kdht_node_copy(remote_id, &contact->remote);

	khashmap_key_t key;
	contact_key(&key, contact->label);
	return kindexedmap_put_allocs(&chat->contacts, &key, contact);
}
contact_t *chat_contact_rm(chat_t *chat, char * name)
{
	khashmap_key_t key;

	contact_key(&key, name);
	return kindexedmap_remove_frees(&chat->contacts, &key);
}

int chat_contact_check_messages(chat_t *chat, contact_t *contact)
{
	kdht_node_t local = { 0 };

	contact_fill_local_node(contact, &local);

	return kdht_service_iterative_findvalue(&chat->dht, &local);
}

int parse_uint64(char *str, uint64_t *dst)
{
	uint64_t val = 0;
	char *endptr = NULL;

	val = strtoul(str, &endptr, 10);
	if (endptr - str != strlen(str)) {
		//this means that part of the string was not a valid number.
		return -1;
	}
	*dst = val;
	return 0;
}
int parse_message(char *line, message_t *message)
{


	char *ptr = line;
	char *id = NULL, *ts = NULL;
	int result = -1;

	if ((id = kstring_next_token_allocs(&ptr, " ")) &&
	    (ts = kstring_next_token_allocs(&ptr, " "))) {
		if (strlen(ptr) > 0) {
			memset(message, 0, sizeof(message_t));
			if (!parse_uint64(id, &message->id) &&
			    !parse_uint64(ts, &message->timestamp_ms)) {

				message->message = strdup(ptr);
				result = 0;
			}
		}
	}

	if (id) {
		free(id);
	}
	if (ts) {
		free(ts);
	}

	return result;
}

void chat_collect_found_answers(kservice_t *service, void *context)
{
	chat_t *chat = (chat_t*)service;

	kdht_rpc_t *rpc = &chat->dht.rpc;
	khashmap_entry_t *entry = NULL;

	KHASHMAP_FOR_EACH_VALUE(rpc->found_values_answers, entry) {
		kdht_value_t *value = (kdht_value_t*)entry->value;
		char *msg_data = (char*)value->bytes;
		message_t message = { 0 };

		if (parse_message(msg_data, &message)) {
			continue;
		} else {
			khashmap_key_t key = { 0 };
			contact_message_container_t* cm = (contact_message_container_t*)khashmap_get(chat->contact_messages, &entry->key);
			if (cm == NULL) {
				cm = calloc(1, sizeof(contact_message_container_t));
				kdht_node_copy(&value->key, &cm->contact_id);
				key.data = (uint8_t*)cm->contact_id.id;
				key.len = sizeof(kdht_node_t);
				khashmap_put_allocs(chat->contact_messages, &entry->key, cm);
				kindexedmap_init_allocs(&cm->messages, chat_message_compare, 1000);
			}
			//now look for message in container.
			key.data = (uint8_t*)&message.id;
			key.len = sizeof(message.id);
			message_t *m = kindexedmap_get(&cm->messages, &key);
			if (m == NULL) {
				m = calloc(1, sizeof(message_t));
				memcpy(m, &message, sizeof(message_t));
				key.data = (uint8_t*)&m->id;
				kindexedmap_put_allocs(&cm->messages, &key, m);
			}



		}

	}
}
contact_message_container_t *chat_contact_get_messages(chat_t *chat, contact_t *contact)
{
	khashmap_key_t key = { 0 };
	kdht_node_t local = { 0 };

	contact_fill_local_node(contact, &local);
	key.data = local.id;
	key.len = DHT_ID_BYTES;
	return khashmap_get(chat->contact_messages, &key);
}

void chat_contact_send_messages_finished(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	kdht_service_iterative_store_value_finished(rpc, call);
	printf("Message stored for contact\n");
}

int chat_contact_send_message(chat_t *chat, contact_t *contact, char *message)
{
	kdht_service_t *dht = &chat->dht;

	//kdht_service_iterative_store_value_finished will look for local value of key = contact->dht_id.
	//klist_node_t *value_node = kdht_table_get_value(&rpc->table.local_values, &call->target);
	kdht_value_t *value = (kdht_value_t*)calloc(1, sizeof(kdht_value_t));

	contact_fill_remote_node(contact, &value->key);

	message_t m = { 0 };
	m.id = rand();
	kdht_rpc_t *rpc = &dht->rpc;
	kservice_t *service = &rpc->service;
	m.timestamp_ms = service->timestamp;
	m.message = message;

	char *msg = 0;
	kstring_sprintf_allocs(&msg, "%lu %lu %s", m.id, m.timestamp_ms, m.message);
	value->bytes = (uint8_t*)msg;
	value->len = strlen(msg);
	kdht_table_insert_value(&dht->rpc.table.local_values, value);

	kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(dht->rpc.service.timestamp, &dht->rpc.table, &dht->rpc.contact_information.node, &dht->rpc.find_node_calls,
									     RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_STORE_VALUE,
									     &value->key,
									     chat_contact_send_messages_finished );
	return kdht_service_iterative_find_node(dht,  &value->key, 0, call);
}
