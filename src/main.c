#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stddef.h>
#include <time.h>
#include <errno.h>

#include <karg.h>
#include <konfig2.h>
#include <knet2_epoll.h>
#include <kdht_service.h>
#include <kfile.h>
#include <kstring.h>


#include "cli.h"
#include "kdht_chat.h"

karg_t args;
static karg_option_t public_network_address;
static karg_option_t listen_address;
static karg_option_t seed_address;
static karg_option_t local_node;
static karg_option_t bootstrap_option;
static karg_option_t config_dir_option;
static char *config_dir = 0, *contacts_file = 0, *config_file = 0;

kdht_node_string_t actual_node_str;

karg_getopt_option_def *init_args_allocs(karg_t *args)
{
	karg_init(args);
	strcpy(args->author, "jkushmaul");
	karg_option_init(&args->options, &bootstrap_option, 'b', (char*)"bootstrap", (char*)"Ignore bootstrap failure", 0, NULL, 0);
	karg_option_init(&args->options, &public_network_address, 'p', (char*)"public", (char*)"Public address for responses", 1, NULL, 1);
	karg_option_init(&args->options, &seed_address, 's', (char*)"seed", (char*)"Seed address", 1, NULL, 1);
	karg_option_init(&args->options, &listen_address, 'l', (char*)"listen", (char*)"Local address to listen. If not provided uses public", 0, NULL, 1);
	karg_option_init(&args->options, &local_node, 'n', (char*)"node", (char*)"Local node id.  If not provided is generated", 0, NULL, 1);
	karg_option_init(&args->options, &config_dir_option, 'c', (char*)"config-dir", (char*)"Config directory, defaults to ~/.config/kdht_chat/", 0, NULL, 1);

	return karg_build_getopts_allocs(args);
}
int parse_args(karg_t *args, int argc, char **argv)
{
	karg_getopt_option_def *def = init_args_allocs(args);
	int parse = karg_parse_args(args, def, argc, argv);

	free(def);
	if (parse < 0) {
		return -1;
	}
	int test = karg_validate_args(args);

	if (test) {
		return -1;
	}

	memset(&actual_node_str, 0, sizeof(actual_node_str));

	if (local_node.value != NULL) {
		if (strlen(local_node.value) != DHT_ID_BYTES * 2) {
			printf("Invalid value for local node\n");
			return -1;
		} else {
			snprintf(actual_node_str.id, sizeof(actual_node_str), "%s", (char*)local_node.value);
		}
	}

	if (config_dir_option.argv_index > 0) {
		config_dir = strdup(config_dir_option.value);
		if (kfile_dir_exists(config_dir)) {
			printf("Configuration directory %s does not exist\n", config_dir);
			return -1;
		}
	} else {
		char *homedir = getenv("HOME");
		kstring_sprintf_allocs(&config_dir, "%s/.config/kdht_chat", homedir);
		if (homedir == NULL) {
			printf("Could not determine home directory\n");
			exit(-1);
		}
	}
	kstring_sprintf_allocs(&contacts_file, "%s/contacts.lst", config_dir);
	kstring_sprintf_allocs(&config_file, "%s/konfig.cfg", config_dir);

	return 0;
}

int init_dht_service(kdht_service_t  *dht, knet2_epoll_handler_t *net,  knet2_error_t *knet2error)
{
	char *local_ip =  listen_address.value;

	if (local_ip == NULL) {
		local_ip = public_network_address.value;
	}
	net->ip_base.listen_backlog = 4096;
	net->ip_base.addr_parse(&net->ip_base.this_addr, local_ip, knet2error);

	if (kdht_node_parse(&actual_node_str, &dht->rpc.contact_information.node) != 0) {
		printf("error parsing %s\n", actual_node_str.id);
		return -1;
	}
	dht->rpc.contact_information.network_address = (char*)public_network_address.value;

	if (kservice_start((kservice_t*)dht, knet2error)) {
		return -1;
	}

	kservice_add_loop_complete_callback((kservice_t*)dht, NULL,  chat_collect_found_answers);

	printf("Local node id: %s\n", actual_node_str.id);

	//bootstrap
	kdht_service_bootstrap_allocs(dht, (const char**)&seed_address.value, 1);
	/*TODO: detect failure
	   if (kdht_service_bootstrap_allocs(dht, (const char**) &seed_address.value, 1)) {
	    return -1;
	   }*/

	return 0;
}


int setup_konfig(konfig2_t *konfig, kdht_service_t *dht)
{
	konfig2_init_allocs(konfig, 1000);
	konfig2_var_new_long_allocs(konfig, "refresh_seconds", &dht->config.refresh_seconds);
	konfig2_var_new_long_allocs(konfig, "network_timeout_seconds", &dht->config.network_timeout_seconds);
	konfig2_var_new_long_allocs(konfig, "expire_seconds", &dht->config.expire_seconds);
	konfig2_var_new_long_allocs(konfig, "replicate_seconds", &dht->config.replicate_seconds);
	konfig2_var_new_long_allocs(konfig, "republish_seconds", &dht->config.republish_seconds);
	kdht_node_string_t *node_str_ptr = &actual_node_str;
	kdht_node_string_t tmp_node_str = { 0 };
	if (local_node.value == NULL) {
		//if no option to set it, use random as default.
		kdht_node_t tmp;
		kdht_node_random(&tmp);
		kdht_node_tostring(&tmp, &actual_node_str);
	} else {
		//this will throw away config value if read.
		node_str_ptr = &tmp_node_str;
	}
	konfig2_var_t *local_node_konfig = konfig2_var_new_string_allocs(konfig, "local_node", node_str_ptr->id, sizeof(kdht_node_string_t));
	//default is a random node


	dht->config.replicate_seconds = 3600;
	dht->config.republish_seconds = 87400;
	dht->config.expire_seconds = 3600;
	dht->config.refresh_seconds = 60;
	dht->config.network_timeout_seconds = 1;

	int result = 0;
	FILE *fp = fopen(config_file, "r");

	if (fp != NULL) {
		char buffer[4096] = { 0 };
		int read = fread(buffer, 1, 4096, fp);
		if (read > 0) {
			if (konfig2_parse_document_allocs(konfig, buffer, sizeof(buffer))) {
				printf("Failed to parse konfig file: %s\n", config_file);
				result = -1;
			}
		} else {
			printf("Failed to read konfig file: %s\n", config_file);
			result = -2;
		}
		fclose(fp);
	}

	local_node_konfig->data = (uint8_t*)actual_node_str.id;
	local_node_konfig->data_len = DHT_ID_BYTES;

	return result;
}

int save_konfig(konfig2_t *konfig)
{
	char *buffer = konfig2_dump(konfig);

	if (buffer) {
		kfile_write_all(config_file, (uint8_t*)buffer, strlen(buffer));
		free(buffer);
	}
	return 0;
}

int parse_contacts(chat_t *chat)
{
	FILE *fp = fopen(contacts_file, "r");

	if (fp != NULL) {
		int i = 0;
		char line[4096];
		while (!feof(fp)) {
			i++;
			char *ptr = fgets(line, 4096, fp);
			if (ptr == NULL) {
				printf("Encountered end of file\n");
				break;
			}

			int count = 0;
			if (line[strlen(line) - 1] != '\n') {
				printf("Line %d was too long\n", i);
				continue;
			}
			line[strlen(line) - 1] = 0;//remove newline
			char **args = kstring_split_allocs(line, " ", &count);
			if (args == NULL) {
				printf("Problem parsing line %d\n", i);
				continue;
			}
			contact_t tmp = { 0 };
			if (count != 3) {
				printf("Line %d has invalid number of fields\n", i);
				continue;
			}
			char *name = args[0];
			if (strlen(name) > sizeof(tmp.label) - 1) {
				printf("Line %d has name which is too long: %s\n", i, name);
				continue;
			}
			char *local_id_str = args[1];
			if (strlen(local_id_str) != sizeof(kdht_node_string_t) - 1 ||
			    kdht_node_parse((kdht_node_string_t*)local_id_str, &tmp.local)
			    ) {
				printf("Line %d has invalid local id %s\n", i, local_id_str);
				continue;
			}
			char *remote_id_str = args[2];
			if (strlen(remote_id_str) != sizeof(kdht_node_string_t) - 1 ||
			    kdht_node_parse((kdht_node_string_t*)remote_id_str, &tmp.remote)
			    ) {
				printf("Line %d has invalid remote id %s\n", i, remote_id_str);
				continue;
			}


			chat_contact_add(chat, name, &tmp.local, &tmp.remote);
			for (int i = 0; i < count; i++) {
				free(args[i]);
			}
			free(args);
		}
		fclose(fp);
	}

	return 0;
}

int save_contacts(chat_t *chat)
{
	if (kfile_mkdir(config_dir)) {
		printf("Could not create config dir\n");
		exit(-1);
	}

	FILE *fp = fopen(contacts_file, "w");
	while (chat->contacts.index.list.head != NULL) {
		klist_node_t *node = chat->contacts.index.list.head;
		contact_t *contact = node->value;


		kdht_node_string_t local_id_str = { 0 };
		kdht_node_string_t remote_id_str = { 0 };
		kdht_node_tostring(&contact->local, &local_id_str);
		kdht_node_tostring(&contact->remote, &remote_id_str);
		fprintf(fp, "%s %s %s\n", contact->label, local_id_str.id, remote_id_str.id);

		chat_contact_rm(chat, contact->label);
	}

	fclose(fp);
	return 0;
}

int main(int argc, char **argv)
{

	knet2_error_t knet2error = { 0 };
	context_t context = { 0 };

	if (parse_args(&args, argc, argv)) {
		karg_print_help(&args);
		exit(-1);
	}

	if (knet2_epoll_handler_init_allocs(&context.net, 1000, &knet2error)) {
		printf("Could not initialize network stack\n");
		exit(-1);
	}
	chat_init(&context.chat, (knet2_ip_handler_t*)&context.net);
	parse_contacts(&context.chat);


	if (setup_konfig(&context.konfig, &context.chat.dht)) {
		printf("Could not parse config\n");
		exit(-1);
	}

	cli_init(&context.cli, &context);

	if (init_dht_service(&context.chat.dht, &context.net, &knet2error)) {
		printf("Could not bootstrap dht service\n");
		if (!bootstrap_option.argv_index) {
			exit(-1);
		}
	}

	kli_start_input_thread(&context.cli);
	kservice_add_loop_complete_callback((kservice_t*)&context.chat.dht, &context, cli_process_thread_input);

	kservice_loop((kservice_t*)&context.chat.dht, &knet2error);

	save_contacts(&context.chat);
	save_konfig(&context.konfig);

	kli_shutdown(&context.cli);

	konfig2_release_frees(&context.konfig);
	karg_release_frees(&args);

	chat_release_frees(&context.chat);
	context.net.ip_base.release_addr(context.chat.dht.rpc.service.network_handler, &context.chat.dht.rpc.service.network_handler->this_addr);


	printf("\n");
	return 0;
}
