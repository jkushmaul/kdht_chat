#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <time.h>
#include <errno.h>

#include <kencode.h>
#include <kdht_cli.h>

#include "cli.h"

#define CLI_F(name) int name(kli_t * cli, void *vcontext, kli_exec_t * exec)



CLI_F(cli_quit) {
	cli->shutdown = 1;
	context_t *context = (context_t*)vcontext;
	kdht_service_t *service = &context->chat.dht;
	service->rpc.service.should_run = 0;
	knet2_error_t error = { 0 };
	knet2_interrupt((knet2_ip_handler_t*)&context->net, &error);
	return 0;
}

CLI_F(cli_clear) {
	void linenoiseClearScreen(void);
	return 0;
}

CLI_F(cli_help) {
	if (cli != NULL) {
		kli_recurse_print_commands(cli, &cli->root_command, 0);
	}

	return 0;
}

CLI_F(cli_friend_add) {
	context_t *chat_context = (context_t*)vcontext;

	if (exec->argc != 1) {
		printf("Missing parameters\n");
		return -1;
	}
	char *name = exec->argv[0];
	contact_t tmp;
	if (strlen(name) > sizeof(tmp.label) - 1) {
		printf("Name is too long\n");
		return -1;
	}

	kdht_node_string_t local_id_str = { 0 };
	kdht_node_random(&tmp.local);
	kdht_node_tostring(&tmp.local, &local_id_str);

	printf("Local ID: %s\n", local_id_str.id);
	printf("Enter Remote ID:");

	char line[sizeof(kdht_node_string_t) + 1] = { 0 };
	if (fgets(line, sizeof(line), stdin) == NULL) {
		printf("Please provide remote ID\n");
		return -1;
	}
	if (line[sizeof(kdht_node_string_t) - 1] != '\n') {
		printf("Remote ID too long\n");
		return -1;
	}
	line[sizeof(kdht_node_string_t) - 1] = 0;//remove newline

	if (kdht_node_parse((kdht_node_string_t*)line, &tmp.remote)) {
		printf("Remote ID was invalid\n");
		return -1;
	}
	contact_t *orig = chat_contact_add(&chat_context->chat, name, &tmp.local, &tmp.remote);

	if (orig != NULL) {
		free(orig);
	}
	printf("Contact %s added\n", name);
	return 0;
}
CLI_F(cli_friend_rm) {
	context_t *chat_context = (context_t*)vcontext;

	if (exec->argc != 1) {
		printf("Missing parameters\n");
		return -1;
	}
	char *name = exec->argv[0];
	contact_t tmp;
	if (strlen(name) > sizeof(tmp.label) - 1) {
		printf("Name is too long\n");
		return -1;
	}

	contact_t * orig = chat_contact_rm(&chat_context->chat, name);
	if (orig) {
		free(orig);
	}

	return 0;
}
CLI_F(cli_friend_ls) {
	context_t *chat_context = (context_t*)vcontext;

	printf("Contacts:\n");
	KLIST_FOR_EACH_NODE(node, &chat_context->chat.contacts.index.list) {
		contact_t *contact = (contact_t*)node->value;

		if (exec->argc == 0 || (exec->argc == 1 && strstr(contact->label, exec->argv[0]) != 0)) {
			kdht_node_string_t local_id_str = { 0 };
			kdht_node_string_t remote_id_str = { 0 };
			kdht_node_tostring(&contact->local, &local_id_str);
			kdht_node_tostring(&contact->remote, &remote_id_str);
			printf("%s: %s %s\n", contact->label, local_id_str.id, remote_id_str.id);
		}
	}

	return 0;
}

CLI_F(cli_inbox_check) {
	context_t *chat_context = (context_t*)vcontext;

	printf("Checking for messages:\n");
	KLIST_FOR_EACH_NODE(node, &chat_context->chat.contacts.index.list) {
		contact_t *contact = (contact_t*)node->value;

		printf("Checking messages for contact[%s]\n", contact->label);
		chat_contact_check_messages(&chat_context->chat, contact);
	}
	printf("Check started...\n");
	return 0;
}

CLI_F(cli_friend_send) {
	context_t *chat_context = (context_t*)vcontext;

	if (exec->argc != 1) {
		printf("Send requires a name\n");
		return -1;
	}
	char *name = exec->argv[0];
	khashmap_key_t key = { 0 };
	contact_key(&key, name);
	contact_t *contact = kindexedmap_get(&chat_context->chat.contacts, &key);
	if (contact == NULL) {
		printf("Contact %s not found\n", name);
		return -1;
	}
	printf("Enter message on one line\n");
	char *message = (char*)calloc(1, 4096);
	char *ptr = fgets(message, 4096, stdin);
	if (ptr == NULL) {
		free(message);
		printf("No input, aborting\n");
		return -1;
	}
	if (ptr[strlen(ptr) - 1] != '\n') {
		free(message);
		printf("Entire line was not read\n");
		return -1;
	}
	ptr[strlen(ptr) - 1] = 0;//remove \n
	chat_contact_send_message(&chat_context->chat, contact, message);
	printf("Storage started\n");
	return 0;
}


CLI_F(cli_friend_read) {
	context_t *chat_context = (context_t*)vcontext;

	if (exec->argc != 1) {
		printf("Read requires a name\n");
		return -1;
	}
	char *name = exec->argv[0];
	khashmap_key_t key = { 0 };
	contact_key(&key, name);
	contact_t *contact = kindexedmap_get(&chat_context->chat.contacts, &key);
	if (contact == NULL) {
		printf("Contact %s not found\n", name);
		return -1;
	}
	printf("Messages from %s\n", name);
	contact_message_container_t *cm = chat_contact_get_messages(&chat_context->chat, contact);
	int i = 0;
	if (cm) {
		KLIST_FOR_EACH_NODE(node, &cm->messages.index.list) {
			i++;
			message_t *m = node->value;
			printf("%lu  at %lu:  %s\n", m->id, m->timestamp_ms, m->message);
		}
	}
	printf("Total %d messages\n", i);
	return 0;
}


CLI_F(cli_inbox_read) {
	context_t *chat_context = (context_t*)vcontext;

	int count = 0;

	KLIST_FOR_EACH_NODE(node, &chat_context->chat.contacts.index.list) {
		contact_t *contact = (contact_t*)node->value;
		contact_message_container_t *cm = chat_contact_get_messages(&chat_context->chat, contact);

		printf("Messages from %s\n", contact->label);
		if (cm) {
			KLIST_FOR_EACH_NODE(node, &cm->messages.index.list) {
				count++;
				message_t *m = node->value;

				printf("%lu  at %lu:  %s\n", m->id, m->timestamp_ms, m->message);
			}
		}
	}

	printf("Total %d messages\n", count);
	return 0;
}

void cli_process_thread_input(kservice_t *service, void *vcontext)
{
	context_t *context = (context_t*)vcontext;
	kli_t *kli = &context->cli;

	kli_process_thread_input(kli);
}



void cli_init(kli_t *cli, context_t *context)
{
	srand(time(NULL));
	kli_init(&context->cli);

	kli_register_command_allocs(cli, NULL, "help", "help", cli_help, context);
	kli_register_command_allocs(cli, NULL, "quit", "quit", cli_quit, context);
	kli_register_command_allocs(cli, NULL, "clear", "clear", cli_clear, context);

	kli_com_t *contact = kli_register_command_allocs(cli, NULL, "contact", "contact", NULL, context);
	kli_register_command_allocs(cli, contact, "add", "contact add", cli_friend_add, context);
	kli_register_command_allocs(cli, contact, "rm", "contact rm <id>", cli_friend_rm, context);
	kli_register_command_allocs(cli, contact, "ls", "contact ls [<name>]", cli_friend_ls, context);
	kli_register_command_allocs(cli, contact, "send", "contact send <name>", cli_friend_send, context);
	kli_register_command_allocs(cli, contact, "read", "contact read <name>", cli_friend_read, context);

	kli_com_t *inbox = kli_register_command_allocs(cli, NULL, "inbox", "inbox", NULL, context);
	kli_register_command_allocs(cli, inbox, "check", "inbox check", cli_inbox_check, context);
	kli_register_command_allocs(cli, inbox, "read", "inbox read", cli_inbox_read, context);

	kli_com_t *dht = kli_register_command_allocs(cli, NULL, "dht", "dht", NULL, context);
	kdht_cli_init(&context->chat.dht, &context->cli, dht);
}
