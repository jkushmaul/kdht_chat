cmake_minimum_required(VERSION 3.5)
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/klibbuilds/cmake
${CMAKE_MODULE_PATH})

set(EXTRA_LIBRARIES  protobuf-c m pthread)

find_package(KlibSetup)
set(UNCRUSTIFY_FILES  ${C_SOURCES} ${C_HEADERS} ${TEST_SOURCES})

enable_testing()


use_klib_project(libktypes 1.0.7 ktypes)

use_klib_project(libknet 1.0.8 knet)

use_klib_project(libkli 1.0.10 kli)

use_klib_project(libkdht2 0.1.6 kdht2)

LIST(APPEND KLIB_DEPS  kdht2 kli ktypes knet  )
setup_klib_executable(kdht_chat ${KLIB_DEPS})

add_dependencies(devbuild kdht_chat-debug)
