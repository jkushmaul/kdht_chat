#include "tests.h"

extern "C" {
#include <chat.h>
#include <knet2_epoll.h>
}

TEST(chat, chat_init) {
	chat_t chat = { 0 };
	knet2_error_t knet2error = { 0 };
	knet2_epoll_handler_t net = { 0 };

	knet2_epoll_handler_init_allocs(&net, 1000, &knet2error);

	chat_init(&chat, (knet2_ip_handler_t*)&net);
	chat_release_frees(&chat);

}
TEST(chat, chat_contact_add) {

}

TEST(chat, chat_contact_rm) {

}

TEST(chat, chat_contact_ls) {

}
