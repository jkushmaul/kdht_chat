#include "tests.h"

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);

	createListener();

	int ret = RUN_ALL_TESTS();

	return ret;
}
